<a name="readme-top"></a>

<br />
<div align="center">
  <h3 align="center">Charlie - Text Extractor</h3>
</div>

<!-- ABOUT THE PROJECT -->
## About The Project

Dopo aver identificato le vignette con il Boss e Dilbert e aver così ottenuto le vignette con entrambi i personaggi e la loro posizione contenuta nel file 'detect_charlie_lucy.txt', si procede innanzitutto con la configurazione del json di autorizzazione per le Vision API (Google Cloud) indicando il json corretto nel file 'extract_text.py'.

Procediamo innanzitutto con la configurazione del json di autorizzazione per l'API Vision (Google Cloud) indicando il json corretto nel file "extract_text.py".
```python
cloud_credentials = 'static-dock-404316-e4fe85985b22.json'
```

A questo punto è possibile lanciare lo script che, se non esiste già, crea un file txt con lo stesso nome dell'immagine, nella cartella 'image_to_parse' con il risultato dell'OCR, che presenta anche il centroide del blocco di testo utile per la successiva fase di costruzione del dataset.
```sh
python3 extract_text_dataset.py
```

```json
[
    {
        "text": "NOWHERE!",
        "centroid": [
            251.5,
            99.5
        ]
    },
    {
        "text": "WHERE DOES THAT LEAVE THE NOWHERE! REST OF US?",
        "centroid": [
            224.25,
            391.5
        ]
    }
]
```

La fase successiva utilizza una misura di distanza per confrontare le coordinate di Charlie e Lucy, presenti nel file 'detect_charlie_lucy.txt', con quelle di ogni singola immagine, facendo corrispondere il testo al personaggio. Viene così generato un file 'dataset.txt' con le "domande e le risposte", cioè il mini-dialogo.
Sono inoltre disponibili diverse funzioni per la pulizia e l'autocorrezione del testo.
```sh
python3 build_text_dataset.py
```


```txt
lucy,"life is a mystery, charlie brown... do you know the answer?"
charlie,why?
lucy,bugs never listen to good advice..
lucy,"charlie brown, youre crazy!"
charlie,"You know, You talk about yourself all the time!"
```


<u><b>In questa repository ci sono solo alcuni esempi.</b></u>

![Charlie & Lucy](charlie-text-extractor/image_to_parse/20160104.png?raw=true "Charlie & Lucy")
<p align="right">(<a href="#readme-top">back to top</a>)</p>
