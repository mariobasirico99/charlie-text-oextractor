import csv
import os
from extract_text import OCR
from pathlib import Path
import json
import math
from tqdm import tqdm
import re
from textblob import Word


def distance(x1, x2, y1, y2):
    return math.hypot(float(x2) - float(x1), float(y2) - float(y1))


def clear_text(text):
    """
    Clears a text string - removes trailing and consecutive spaces, characters other than letters and interpunction, and
    converts the string to lowercase.
    :param text: String to process.
    :return: Cleared strin.
    """
    text = text.lower()

    text = re.sub(r'[^a-zA-Z.,?!\s]+', '', text)
    _RE_COMBINE_WHITESPACE = re.compile(r"\s+")
    text = _RE_COMBINE_WHITESPACE.sub(" ", text).strip()

    return text


def autocorrect_text(text):
    """
    Uses autocorrect to correct the text.
    :param text: String to correct.
    :return: Corrected string.
    """
    words = text.split(' ')
    corrected = []
    for word in words:
        gfg = Word(word)
        spellcheck = gfg.spellcheck()
        if 0.75 < spellcheck[0][1] < 1:
            word = spellcheck[0][0]

        corrected.append(word)

    text = ' '.join(corrected)
    return clear_text(text)


if __name__ == "__main__":

    with open('detect_charlie_lucy.txt') as f:
        file = open('datasetd.csv', 'w', encoding='UTF8', newline='')
        # create the csv writer
        writer = csv.writer(file)
        for line in tqdm(f):
            line = line.strip().split(" ")

            charlie_centroid = [line[1], line[2]]
            lucy_centroid = [line[3], line[4]]

            charlie_text = ""
            lucy_text = ""

            path = os.path.join("image_to_parse/", line[0])
            transcription_filename = Path(path).with_suffix('.txt')
            if (os.path.isfile(transcription_filename)):
                print("transcription_filename", transcription_filename)
                f = open(transcription_filename)
                json_object = json.load(f)
                for text in json_object:

                    charlie_distance = abs(
                        distance(charlie_centroid[0], text['centroid'][0], charlie_centroid[1], text['centroid'][1]))
                    lucy_distance = abs(
                        distance(lucy_centroid[0], text['centroid'][0], lucy_centroid[1], text['centroid'][1]))

                    charlie_x_distance = abs(float(text['centroid'][0]) - float(charlie_centroid[0]))
                    lucy_x_distance = abs(float(text['centroid'][0]) - float(lucy_centroid[0]))
                    if charlie_distance > lucy_distance:
                        lucy_text = lucy_text + " " + text['text']
                    else:
                        charlie_text = charlie_text + " " + text['text']

                f.close()
                charlie_text = ["charlie", autocorrect_text(charlie_text)]
                lucy_text = ["lucy", autocorrect_text(lucy_text)]

                if (float(charlie_centroid[0]) < float(lucy_centroid[0])):
                    if charlie_text[1] != "":
                        writer.writerow(charlie_text)
                    if lucy_text[1] != "":
                        writer.writerow(lucy_text)
                else:
                    if lucy_text[1] != "":
                        writer.writerow(lucy_text)
                    if charlie_text[1] != "":
                        writer.writerow(charlie_text)

        file.close()
